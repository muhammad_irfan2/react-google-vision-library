package com.btpn.lib.googlevision.module.scanner;

import com.btpn.lib.googlevision.module.base.BaseModuleImpl;
import com.btpn.lib.googlevision.service.scanner.ScannerServiceImpl;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactMethod;

/**
 * Created by anka on 12/18/17.
 */

public class ScannerModuleImpl extends BaseModuleImpl implements ScannerModule {

    private ScannerServiceImpl mScannerService;

    private static final String TAG = ScannerModuleImpl.class.getSimpleName();

    public ScannerModuleImpl(ReactApplicationContext reactContext, String name) {
        super(reactContext, name);

        mScannerService = new ScannerServiceImpl(reactContext);
    }

    @ReactMethod
    @Override
    public void openBarcodeScanner(Promise promise) {
        mScannerService.openBarcodeScanner(promise);
    }
}
