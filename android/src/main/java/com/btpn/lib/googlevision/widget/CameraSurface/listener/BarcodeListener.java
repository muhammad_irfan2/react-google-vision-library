package com.btpn.lib.googlevision.widget.CameraSurface.listener;

import com.google.android.gms.vision.barcode.Barcode;

/**
 * Created by Anka.Wirawan on 7/9/2015.
 */
public interface BarcodeListener {

    void onBarcodeDetected(Barcode barcode);
}
