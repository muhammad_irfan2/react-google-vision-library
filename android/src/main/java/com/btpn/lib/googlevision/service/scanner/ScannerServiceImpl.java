package com.btpn.lib.googlevision.service.scanner;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.btpn.lib.googlevision.activity.ScannerActivity;
import com.btpn.lib.googlevision.model.Response;
import com.btpn.lib.googlevision.service.base.BaseServiceImpl;
import com.btpn.lib.googlevision.util.Constants;
import com.facebook.react.ReactActivity;
import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.modules.core.PermissionListener;

/**
 * Created by anka on 12/24/17.
 */

public class ScannerServiceImpl extends BaseServiceImpl implements ScannerService, ActivityEventListener, PermissionListener {

    private static final String TAG = ScannerServiceImpl.class.getSimpleName();
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 101;
    private static final int REQUEST_CODE_START_BARCODE_SCANNER = 102;

    public ScannerServiceImpl(ReactContext context) {
        super(context);

        context.addActivityEventListener(this);
    }

    private boolean isCameraAvailable() {
        return getContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)
                || getContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY);
    }

    private void requestPermission() {

        if (!ActivityCompat.shouldShowRequestPermissionRationale(getContext().getCurrentActivity(),
                Manifest.permission.CAMERA)) {
            ((ReactActivity) getContext().getCurrentActivity()).requestPermissions(
                    new String[] {Manifest.permission.CAMERA},
                    REQUEST_CODE_ASK_PERMISSIONS,
                    this);
        }

        ((ReactActivity) getContext().getCurrentActivity()).requestPermissions(
                new String[] {Manifest.permission.CAMERA},
                REQUEST_CODE_ASK_PERMISSIONS,
                this);
    }

    private void launchBarcodeReader() {
        Intent intent = new Intent(this.getContext(), ScannerActivity.class);
        (getContext().getCurrentActivity()).startActivityForResult(intent, REQUEST_CODE_START_BARCODE_SCANNER, null);
    }

    @Override
    public void openBarcodeScanner(Promise promise) {
        super.setPromise(promise);

        if (isCameraAvailable()) {
            int hasPermission = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA);

            if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermission();
            } else {
                launchBarcodeReader();
            }
        } else {
            super.sendPromise(new Response(Response.SDK_RESPONSE_INTERNAL_ERROR, null, "Camera not available.", false));
        }
    }

    @Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_START_BARCODE_SCANNER) {
            if (resultCode == Activity.RESULT_OK) {
                String barcode = data.getStringExtra(Constants.BARCODE);
                super.sendPromise(new Response(Response.SDK_RESPONSE_OK, barcode, "Scan completed successfully", true));
            } else {
                super.sendPromise(new Response(Response.SDK_RESPONSE_INTERNAL_ERROR, null, "Scan canceled.", false));
            }
        }
    }

    @Override
    public void onNewIntent(Intent intent) {

    }

    @Override
    public boolean onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        boolean granted = false;

        if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                granted = true;
                launchBarcodeReader();
            }
        }

        if (!granted) {
            super.sendPromise(new Response(Response.SDK_RESPONSE_INTERNAL_ERROR, null, "Access camera denied", false));
        }

        return granted;
    }
}
