package com.btpn.lib.googlevision.widget.CameraSurface;

import android.graphics.Rect;
import android.hardware.Camera;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;

import com.google.android.gms.vision.CameraSource;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anka.Wirawan on 9/23/2015.
 */
public class VisionApiFocusFix {

    /*
 * IF YOU WANT TO JUST ACCESS THE CAMERA INSTANCE SO THAT YOU CAN SET ANY OF THE PARAMETERS, VISIT THE FOLLOWING LINK:
 * https://gist.github.com/Gericop/364dd12b105fdc28a0b6
 */

    /**
     * Custom annotation to allow only valid focus modes.
     */
    @StringDef({
            Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE,
            Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO,
            Camera.Parameters.FOCUS_MODE_AUTO,
            Camera.Parameters.FOCUS_MODE_EDOF,
            Camera.Parameters.FOCUS_MODE_FIXED,
            Camera.Parameters.FOCUS_MODE_INFINITY,
            Camera.Parameters.FOCUS_MODE_MACRO
    })
    @Retention(RetentionPolicy.SOURCE)
    private @interface FocusMode {}

    /**
     * <p>
     * Sets the Mobile Vision API provided {@link com.google.android.gms.vision.CameraSource}'s
     * focus mode. Use {@link Camera.Parameters#FOCUS_MODE_CONTINUOUS_PICTURE} or
     * {@link Camera.Parameters#FOCUS_MODE_CONTINUOUS_VIDEO} for continuous autofocus.
     * </p>
     * <p>
     * Note that the CameraSource's {@link CameraSource#start()} or
     * {@link CameraSource#start(SurfaceHolder)} has to be called and the camera image has to be
     * showing prior using this method as the CameraSource only creates the camera after calling
     * one of those methods and the camera is not available immediately. You could implement some
     * kind of a callback method for the SurfaceHolder that notifies you when the imaging is ready
     * or use a direct action (e.g. button press) to set the focus mode.
     * </p>
     * <p>
     * Check out <a href="https://github.com/googlesamples/android-vision/blob/master/face/multi-tracker/app/src/main/java/com/google/android/gms/samples/vision/face/multitracker/ui/camera/CameraSourcePreview.java#L84">CameraSourcePreview.java</a>
     * which contains the method <code>startIfReady()</code> that has the following line:
     * <blockquote><code>mCameraSource.start(mSurfaceView.getHolder());</code></blockquote><br>
     * After this call you can use our <code>cameraFlashLight(...)</code> method because the camera is ready.
     * </p>
     *
     * @param cameraSource The CameraSource built with {@link com.google.android.gms.vision.CameraSource.Builder}.
     * @param focusMode    The focus mode. See {@link Camera.Parameters} for possible values.
     * @return true if the camera's focus is set; false otherwise.
     * @see com.google.android.gms.vision.CameraSource
     * @see Camera.Parameters
     */
    public static boolean cameraFocus(@NonNull CameraSource cameraSource, @FocusMode @NonNull String focusMode, Rect rect, Rect meteringRect) {
        Field[] declaredFields = CameraSource.class.getDeclaredFields();

        for (Field field : declaredFields) {
            if (field.getType() == Camera.class) {
                field.setAccessible(true);
                try {
                    Camera camera = (Camera) field.get(cameraSource);
                    if (camera != null) {

                        camera.cancelAutoFocus();
                        camera.autoFocus(null);

                        Camera.Parameters params = camera.getParameters();

                        if (!params.getSupportedFocusModes().contains(focusMode)) {
                            return false;
                        }

                        List<Camera.Area> areas = new ArrayList<>();
                        areas.add(new Camera.Area(rect, 1000));

                        List<Camera.Area> meterings = new ArrayList<>();
                        meterings.add(new Camera.Area(meteringRect, 1000));

                        params.setFocusMode(focusMode);
                        params.setFocusAreas(areas);
                        params.setMeteringAreas(meterings);

                        camera.setParameters(params);
                        return true;
                    }

                    return false;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            }
        }

        return false;
    }
}
