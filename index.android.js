'use strict'

import { NativeModules } from 'react-native';

const { 
    GoogleVisionScanner
} = NativeModules;

export {    
    GoogleVisionScanner
}