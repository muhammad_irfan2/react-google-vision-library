package com.btpn.lib.googlevision.module.scanner;

import com.facebook.react.bridge.Promise;

/**
 * Created by anka on 12/18/17.
 */

public interface ScannerModule {
    void openBarcodeScanner(Promise promise);
}
