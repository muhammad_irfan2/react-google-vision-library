package com.btpn.lib.googlevision.service.scanner;

import com.facebook.react.bridge.Promise;

/**
 * Created by anka on 12/24/17.
 */

public interface ScannerService {
    void openBarcodeScanner(Promise promise);
}
