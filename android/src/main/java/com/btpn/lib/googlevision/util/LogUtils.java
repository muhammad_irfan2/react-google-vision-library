package com.btpn.lib.googlevision.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by anka on 9/24/17.
 */

public class LogUtils {

    private LogUtils() {}

    public static void log(@NonNull int type, @NonNull String tag, @NonNull String message) {
        if (!TextUtils.isEmpty(message)) {
            switch (type) {
                case Log.VERBOSE:
                    Log.v(tag, message);
                    break;
                case Log.INFO:
                    Log.i(tag, message);
                    break;
                case Log.WARN:
                    Log.w(tag, message);
                    break;
                case Log.ERROR:
                    Log.e(tag, message);
                    break;
                default:
                    Log.d(tag, message);
            }
        }
    }

    public static void showToast(Context context, int messageId, int duration) {
        Toast.makeText(context, context.getString(messageId), duration).show();
    }

    public static void showToast(Context context, String message, int duration) {
        Toast.makeText(context, message, duration).show();
    }
}
