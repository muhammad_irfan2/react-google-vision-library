package com.btpn.lib.googlevision.module.base;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;



/**
 * Created by anka on 9/23/17.
 */

public class BaseModuleImpl extends ReactContextBaseJavaModule implements BaseModule {

    private ReactApplicationContext mReactContext;
    private String mName;

    public BaseModuleImpl(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    public BaseModuleImpl(ReactApplicationContext reactContext, String name) {
        super(reactContext);

        this.mReactContext = reactContext;
        this.mName = name;
    }

    @Override
    public ReactApplicationContext getContext() {
        return mReactContext;
    }

    @Override
    public void setName(String name) {
        this.mName = name;
    }

    @Override
    public String getName() {
        return this.mName;
    }
}
