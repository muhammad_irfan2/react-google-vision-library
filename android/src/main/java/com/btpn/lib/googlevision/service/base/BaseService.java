package com.btpn.lib.googlevision.service.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.btpn.lib.googlevision.model.Response;

/**
 * Created by anka on 9/22/17.
 */

public interface BaseService {
    void sendEvent(@NonNull String event, @Nullable Response response);
    void sendPromise(@Nullable Response response);
}
